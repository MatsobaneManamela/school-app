import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import {Router} from'@angular/router';
import {NgForm} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {Student} from '../Models/student';
import {StudentService} from '../Service/student.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  student : Student;
  isLoginError : boolean = false;
  constructor(private studentservice : StudentService, private router : Router, private toastr : ToastrService) { }

  ngOnInit(): void {
    this.resetForm();
  }

  resetForm(form? : NgForm){

    if(form != null)
    form.reset();
    this.student = {
      StudentNumber: 0,
      Name: '', 
      Surname : '',
      IDNumber: 0, 
      Gender : '',
      MobileNumbers: '',
      HomeAddress : '',
      Suburb : '',
      City : '',
      Province: '', 
      ZIP : '',
      Grade : '', 
      Password : '',
      Email : '', 
      NameOfSchool : '' 
    }
  }

  OnSubmit(Email,Password){
 
    this.studentservice.userAuthentication(Email,Password).subscribe((data : any)=>{
    localStorage.setItem('userToken', data.access_token);
    this.router.navigate(['/homepage']);
    this.toastr.success('Welcome');
    },
       
    (err : HttpErrorResponse)=>{
    this.isLoginError = true;
    this.toastr.error('incorrect Student Number and Password');
    });
    }


}
