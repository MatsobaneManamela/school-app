import { TestBed } from '@angular/core/testing';

import { StudyGuideGrade11Service } from './study-guide-grade11.service';

describe('StudyGuideGrade11Service', () => {
  let service: StudyGuideGrade11Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StudyGuideGrade11Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
