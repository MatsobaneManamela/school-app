import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {Assignment} from '../Models/assignment';

@Injectable({
  providedIn: 'root'
})
export class AssignmentService {
assignment : Assignment;
readonly rootUrl = "http://localhost:64567/";
  constructor(private httpclient :HttpClient ) { }

  PostAssignment(assignment : Assignment){
    var body = JSON.stringify(assignment);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpclient.post(this.rootUrl + 'api/Assignments', body,{headers : headersOption} );
  }

  getassignment(data:any): Observable<Assignment[]>
  {
    return this.httpclient.get<Assignment[]>(this.rootUrl+'api/GetAssignments?id='+data);
  }

Updateassignment(id,assignment){
 var body = JSON.stringify(assignment);
var headersOption = new HttpHeaders({'Content-Type':'application/json'});

 return this.httpclient.post(this.rootUrl + 'api/Assignments/'+id, body, {headers : headersOption});
  }

  Deleteassignment(id : number)
  {
    return this.httpclient.delete(this.rootUrl + 'api/Assignments/'+id);
  }
}
