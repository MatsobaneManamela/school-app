import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {StudentBooksGrade11} from '../Models/student-books-grade11';

@Injectable({
  providedIn: 'root'
})
export class StudentBooksGrade11Service {

  readonly rootUrl = "http://localhost:64567/";

  studentbooksgrade11: StudentBooksGrade11;

  constructor(private httpClient : HttpClient) { }


      getstudentbooksgrade11(data:any): Observable<StudentBooksGrade11[]>
      {
        return this.httpClient.get<StudentBooksGrade11[]>(this.rootUrl+'api/GetStudent_Books_Grade11?id='+data);
      }

}
