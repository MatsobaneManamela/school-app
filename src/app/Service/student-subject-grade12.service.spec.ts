import { TestBed } from '@angular/core/testing';

import { StudentSubjectGrade12Service } from './student-subject-grade12.service';

describe('StudentSubjectGrade12Service', () => {
  let service: StudentSubjectGrade12Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StudentSubjectGrade12Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
