import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {StudentBooksGrade10} from '../Models/student-books-grade10';

@Injectable({
  providedIn: 'root'
})
export class StudentBooksGrade10Service {

  readonly rootUrl = "http://localhost:64567/";

  studentbooksgrade10: StudentBooksGrade10;

  constructor(private httpClient : HttpClient) { }

      getstudentbooksgrade10(data:any): Observable<StudentBooksGrade10[]>
      {
        return this.httpClient.get<StudentBooksGrade10[]>(this.rootUrl+'api/GetStudent_Books_Grade10?id='+data);
      }

}
