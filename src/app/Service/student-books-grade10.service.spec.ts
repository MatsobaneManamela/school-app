import { TestBed } from '@angular/core/testing';

import { StudentBooksGrade10Service } from './student-books-grade10.service';

describe('StudentBooksGrade10Service', () => {
  let service: StudentBooksGrade10Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StudentBooksGrade10Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
