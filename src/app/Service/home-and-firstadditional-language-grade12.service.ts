import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {HomeAndFirstAdditionalLanguageGrade12} from '../Models/home-and-first-additional-language-grade12';

@Injectable({
  providedIn: 'root'
})
export class HomeAndFirstadditionalLanguageGrade12Service {

  readonly rootUrl = "http://localhost:64567/";

  homeandfirstadditionallanguagegrade12: HomeAndFirstAdditionalLanguageGrade12;
  selectedhomeandfirstadditionallanguagegrade12 : HomeAndFirstAdditionalLanguageGrade12; 
  homeandfirstadditionallanguagegrade12list : HomeAndFirstAdditionalLanguageGrade12[];
  allhomeandfirstadditionallanguagegrade12list : HomeAndFirstAdditionalLanguageGrade12[];

  constructor(private httpClient : HttpClient) { }


      gethomeandfirstadditionallanguagegrade12(data:any): Observable<HomeAndFirstAdditionalLanguageGrade12[]>
      {
        return this.httpClient.get<HomeAndFirstAdditionalLanguageGrade12[]>(this.rootUrl+'api/GetHome_And_FirstAdditional_Language_Grade12byID?id='+data);
      }

}
