import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {HomeAndFirstAdditionalLanguageGrade11} from '../Models/home-and-first-additional-language-grade11';

@Injectable({
  providedIn: 'root'
})
export class HomeAndFirstadditionalLanguageGrade11Service {

  readonly rootUrl = "http://localhost:64567/";

  homeandfirstadditionallanguagegrade11: HomeAndFirstAdditionalLanguageGrade11;

  constructor(private httpClient : HttpClient) { }

      gethomeandfirstadditionallanguagegrade11(data:any): Observable<HomeAndFirstAdditionalLanguageGrade11[]>
      {
        return this.httpClient.get<HomeAndFirstAdditionalLanguageGrade11[]>(this.rootUrl+'api/GetHome_And_FirstAdditional_Language_Grade11byID?id='+data);
      }
}
