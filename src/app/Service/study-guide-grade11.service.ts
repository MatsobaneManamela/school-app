import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {StudentStudyGuideGrade11} from '../Models/student-study-guide-grade11';

@Injectable({
  providedIn: 'root'
})
export class StudyGuideGrade11Service {

  readonly rootUrl = "http://localhost:64567/";

  studentstudyguidegrade11: StudentStudyGuideGrade11;

  constructor(private httpClient : HttpClient) { }

      getstudentstudyguidegrade11(data:any): Observable<StudentStudyGuideGrade11[]>
      {
        return this.httpClient.get<StudentStudyGuideGrade11[]>(this.rootUrl+'api/GetStudent_StudyGuide_Grade11?id='+data);
      }

}
