import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {StudentBooksGrade12} from '../Models/student-books-grade12';

@Injectable({
  providedIn: 'root'
})
export class StudentBooksGrade12Service {

  readonly rootUrl = "http://localhost:64567/";

  studentbooksgrade12: StudentBooksGrade12;
  constructor(private httpClient : HttpClient) { }

      getstudentbooksgrade12(data:any): Observable<StudentBooksGrade12[]>
      {
        return this.httpClient.get<StudentBooksGrade12[]>(this.rootUrl+'api/GetStudent_Books_Grade12?id='+data);
      }
}
