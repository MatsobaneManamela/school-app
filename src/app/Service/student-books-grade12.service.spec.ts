import { TestBed } from '@angular/core/testing';

import { StudentBooksGrade12Service } from './student-books-grade12.service';

describe('StudentBooksGrade12Service', () => {
  let service: StudentBooksGrade12Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StudentBooksGrade12Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
