import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import{Guardian} from '../Models/guardian';

@Injectable({
  providedIn: 'root'
})
export class GuardianService {
  readonly rootUrl = "http://localhost:64567/";

  guadian: Guardian;

  constructor(private httpClient : HttpClient) { }

   getGuardians(data:any): Observable<Guardian[]>
      {
        return this.httpClient.get<Guardian[]>(this.rootUrl+'api/GetGuardian?id='+data);
      }
}
