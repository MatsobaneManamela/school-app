import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {News} from '../Models/news';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
 news : News ;
  readonly rootUrl = "http://localhost:64567/";
  constructor(private httpclient :HttpClient ) { }

  PostNews(news : News){
    var body = JSON.stringify(news);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpclient.post(this.rootUrl + 'api/News', body,{headers : headersOption} );
  }
  getNews() :Observable<News[]>{

    return this.httpclient.get<News[]>(this.rootUrl+'api/News');
  }
  getNewsByID(data:any): Observable<News[]>
  {
    return this.httpclient.get<News[]>(this.rootUrl+'api/News/'+data);
  }

UpdateNews(id,news){
 var body = JSON.stringify(news);
var headersOption = new HttpHeaders({'Content-Type':'application/json'});

 return this.httpclient.post(this.rootUrl + 'api/News/'+id, body, {headers : headersOption});
  }

  DeleteNews(id : number)
  {
    return this.httpclient.delete(this.rootUrl + 'api/News/'+id);
  }
}
