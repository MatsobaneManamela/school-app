import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
//import{Http,Response, Headers,RequestOptions, RequestMethod} from '@angular/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {HomeAndFirstAdditionalLanguageGrade10} from '../Models/home-and-first-additional-language-grade10';

@Injectable({
  providedIn: 'root'
})
export class HomeAndFirstadditionalLanguageGrade10Service {

  readonly rootUrl = "http://localhost:64567/";

  homeandfirstadditionallanguagegrade10: HomeAndFirstAdditionalLanguageGrade10;


  constructor(private httpClient : HttpClient) { }

 
      getHomeAndFirstAdditionalLanguageGrade10(data:any): Observable<HomeAndFirstAdditionalLanguageGrade10[]>
      {
        return this.httpClient.get<HomeAndFirstAdditionalLanguageGrade10[]>(this.rootUrl+'api/GetHome_And_FirstAdditional_Language_Grade10byID?id='+data);
      }

      
}
