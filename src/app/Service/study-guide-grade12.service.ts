import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
//import{Http,Response, Headers,RequestOptions, RequestMethod} from '@angular/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {StudentStudyGuideGrade12} from '../Models/student-study-guide-grade12';

@Injectable({
  providedIn: 'root'
})
export class StudyGuideGrade12Service {

  readonly rootUrl = "http://localhost:64567/";

  studentstudyguidegrade12: StudentStudyGuideGrade12;

  constructor(private httpClient : HttpClient) { }

      getstudentstudyguidegrade12(data:any): Observable<StudentStudyGuideGrade12[]>
      {
        return this.httpClient.get<StudentStudyGuideGrade12[]>(this.rootUrl+'api/GetStudent_GuideBook_Grade12?id='+data);
      }
}
