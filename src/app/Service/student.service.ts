import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {Student} from '../Models/student';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  readonly rootUrl = "http://localhost:64567/";

  student: Student;


  constructor(private httpClient : HttpClient) { }

  PostStudent(student: Student){
    var body = JSON.stringify(student);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Students', body, {headers : headersOption});
    }

    userAuthentication(UserName, Password){
      var data = "username="+UserName+"&password="+Password+"&grant_type=password";
      var reqHeader = new HttpHeaders({'Content-Type':'application/x-www-urlencoded'});
      return this.httpClient.post(this.rootUrl+'/token',data, {headers: reqHeader});
    }

    getStudentClaims(){
      return this.httpClient.get(this.rootUrl+'api/GetStudentClaims',{headers : new HttpHeaders({'Authorization':'Bearer '+localStorage.getItem('userToken')})});
     }

    UpdateStudent(id,student){
     var body = JSON.stringify(student);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Students/'+id, body, {headers : headersOption});
      }
}
