import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
//import{Http,Response, Headers,RequestOptions, RequestMethod} from '@angular/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {StudentSubjectGrade11} from '../Models/student-subject-grade11';

@Injectable({
  providedIn: 'root'
})
export class StudentSubjectGrade11Service {

  readonly rootUrl = "http://localhost:64567/";

  studentsubjectgrade11: StudentSubjectGrade11;

  constructor(private httpClient : HttpClient) { }

      getStudentSubjectGrade11(data:any): Observable<StudentSubjectGrade11[]>
      {
        return this.httpClient.get<StudentSubjectGrade11[]>(this.rootUrl+'api/GetStudent_Subject_Grade11?id='+data);
      }
}
