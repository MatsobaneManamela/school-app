import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {StudentSubjectGrade12} from '../Models/student-subject-grade12';

@Injectable({
  providedIn: 'root'
})
export class StudentSubjectGrade12Service {

  readonly rootUrl = "http://localhost:64567/";

  studentsubjectgrade12: StudentSubjectGrade12;

  constructor(private httpClient : HttpClient) { }


      getStudentSubjectGrade12(data:any): Observable<StudentSubjectGrade12[]>
      {
        return this.httpClient.get<StudentSubjectGrade12[]>(this.rootUrl+'api/GetStudent_Subject_Grade12?id='+data);
      }
}
