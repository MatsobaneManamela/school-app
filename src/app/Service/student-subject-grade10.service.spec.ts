import { TestBed } from '@angular/core/testing';

import { StudentSubjectGrade10Service } from './student-subject-grade10.service';

describe('StudentSubjectGrade10Service', () => {
  let service: StudentSubjectGrade10Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StudentSubjectGrade10Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
