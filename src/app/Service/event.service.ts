import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {Event} from '../Models/event';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  event : Event ;
  readonly rootUrl = "http://localhost:64567/";
  constructor(private httpclient :HttpClient ) { }

  PostEvent(event : Event ){
    var body = JSON.stringify(event);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpclient.post(this.rootUrl + 'api/Events', body,{headers : headersOption} );
  }
  getevent() :Observable<Event[]>{

    return this.httpclient.get<Event[]>(this.rootUrl+'api/Events');
  }
  getEventByID(data:any): Observable<Event[]>
  {
    return this.httpclient.get<Event[]>(this.rootUrl+'api/Events/'+data);
  }

UpdateEvent(id,event){
 var body = JSON.stringify(event);
var headersOption = new HttpHeaders({'Content-Type':'application/json'});

 return this.httpclient.post(this.rootUrl + 'api/Events/'+id, body, {headers : headersOption});
  }

  DeleteEvent(id : number)
  {
    return this.httpclient.delete(this.rootUrl + 'api/Events/'+id);
  }
}
