import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {Activity} from '../Models/activity';

@Injectable({
  providedIn: 'root'
})
export class ActivitiesService {
  activity : Activity;
  readonly rootUrl = "http://localhost:64567/";
  constructor(private httpClient : HttpClient) { }

  PostActivity(activity : Activity){
    var body = JSON.stringify(activity);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Activities', body, {headers : headersOption});
    }

      getActivities(data:any): Observable<Activity[]>
      {
        return this.httpClient.get<Activity[]>(this.rootUrl+'api/GetActivities?id='+data);
      }

    UpdateActivities(id,activity){
     var body = JSON.stringify(activity);
    var headersOption = new HttpHeaders({'Content-Type':'application/json'});
    
     return this.httpClient.post(this.rootUrl + 'api/Activities/'+id, body, {headers : headersOption});
      }

      DeleteActivities(id : number)
      {
        return this.httpClient.delete(this.rootUrl + 'api/Activities/'+id);
      }
}
