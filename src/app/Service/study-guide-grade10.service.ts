import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest,HttpResponse,HttpHeaderResponse} from '@angular/common/http';
import { Observable, Subject, ReplaySubject, from, of, range, BehaviorSubject} from 'rxjs';
import { map,mergeMap} from 'rxjs/operators';
import {StudentStudyGuideGrade10} from '../Models/student-study-guide-grade10';

@Injectable({
  providedIn: 'root'
})
export class StudyGuideGrade10Service {

  readonly rootUrl = "http://localhost:64567/";

  studentstudyguidegrade10: StudentStudyGuideGrade10;

  constructor(private httpClient : HttpClient) { }

      getstudentstudyguidegrade10(data:any): Observable<StudentStudyGuideGrade10[]>
      {
        return this.httpClient.get<StudentStudyGuideGrade10[]>(this.rootUrl+'api/GetStudent_StudyGuide_Grade10?id='+data);
      }

}
