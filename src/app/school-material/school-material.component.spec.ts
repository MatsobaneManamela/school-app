import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchoolMaterialComponent } from './school-material.component';

describe('SchoolMaterialComponent', () => {
  let component: SchoolMaterialComponent;
  let fixture: ComponentFixture<SchoolMaterialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchoolMaterialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchoolMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
