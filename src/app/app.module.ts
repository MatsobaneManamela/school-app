import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomepageComponent } from './homepage/homepage.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { LibraryComponent } from './library/library.component';
import { SchoolMaterialComponent } from './school-material/school-material.component';
import { EventComponent } from './event/event.component';
import { MessageComponent } from './message/message.component';
import { ActivitiesComponent } from './activities/activities.component';
import { TutorialsComponent } from './tutorials/tutorials.component';
import { SchoolNewsComponent } from './school-news/school-news.component';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    LoginComponent,
    ProfileComponent,
    LibraryComponent,
    SchoolMaterialComponent,
    EventComponent,
    MessageComponent,
    ActivitiesComponent,
    TutorialsComponent,
    SchoolNewsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
