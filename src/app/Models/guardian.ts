export class Guardian {
    ID :number;
    StudentNumber: number;
     Name : string;
     Surname :string;
    IDNumber :string;
     Gender :string;
     MobileNumber: string;
     HomeNumber: string;
     PhysicalAddress:string;
     Suburb: string;
     City: string;
     Province: string;
     ZIP : string;
     Relation : string;

}
