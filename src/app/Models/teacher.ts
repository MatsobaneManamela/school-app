export class Teacher {
    TeacherID : number;
    Name: string;
    Surname :string;
    IDNumber: string;
    Gender: string;
    Email: string;
    Password :string;
    Position : string;
    MobileNumber: string;
    HomeNumber :string;
    Specialization : string;
	School_Name : string;
}
