export class Assignment {
        AssignmentID : number;
        TeacherID : number;
        Subject : string
        Grade : string
        File : string
        Submission_date : string;
        Date_and_Time : string;
}
