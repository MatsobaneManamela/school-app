export class HomeAndFirstAdditionalLanguageGrade11 {
    LanguageID : number;
    StudentNumber: number;
    HomeLanguageBook1 :string
    HomeLanguageBook2 : string;
    HomeLanguageBook3 :string;
    FirstAdditionalLanguageBook1: string;
    FirstAdditionalLanguageBook2 : string;
    FirstAdditionalLanguageBook3 :string;
}
