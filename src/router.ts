import {Routes} from '@angular/router';
import {HomepageComponent} from './app/homepage/homepage.component';
import {AuthGuard} from './app/Auth/auth.guard.service';
import {LoginComponent} from './app/login/login.component';
import {ActivitiesComponent} from './app/activities/activities.component';
import {EventComponent} from './app/event/event.component';
import {LibraryComponent} from './app/library/library.component';
import {MessageComponent} from './app/message/message.component';
import {ProfileComponent} from './app/profile/profile.component';
import {SchoolMaterialComponent} from './app/school-material/school-material.component';
import {TutorialsComponent} from './app/tutorials/tutorials.component';
import {SchoolNewsComponent} from './app/school-news/school-news.component';


export const appRoutes : Routes =[
    {path: 'homepage', component : HomepageComponent},
    {path : 'login', component : LoginComponent},
    {path: 'activity', component : ActivitiesComponent},
    {path: 'Event', component : EventComponent},
    {path: 'Library', component : LibraryComponent},
    {path: 'message', component : MessageComponent},
    {path: 'Profile', component : ProfileComponent},
    {path: 'schoolmaterial', component : SchoolMaterialComponent},
    {path: 'Tutorials', component : TutorialsComponent},
    {path: 'News', component : SchoolNewsComponent},
    {path: '',redirectTo: '/homepage', pathMatch : 'full'}

];;
